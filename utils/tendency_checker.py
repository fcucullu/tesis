import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from utils.candlestick import CandlestickRepository
from datetime import datetime, timedelta
import warnings
warnings.filterwarnings('ignore')

try:
    CANDLES = CandlestickRepository.default_repository()
except:
    CANDLES = CandlestickRepository.preprod_repository()
BULL_MARKET = 'bull_market'
BEAR_MARKET = 'bear_market'
LATERAL_MARKET = 'lateral_market'

MINUTES_IN_OBS = 60 * 4
INTERVAL_WIDTH_IN_DAYS = 1
TODAY = datetime.utcnow()
DRIFT_THRESHOLD = 0.001
METHOD = 'arithmetic'

class TendencyChecker():

    def __init__(self):
        self.candlestick_repo = CANDLES

    def linreg(self, timeserie, method='arithmetic'):
        if method == 'arithmetic':
            y = (timeserie.pct_change().cumsum() + 1).reset_index(drop=True).replace(np.nan, 1).rename('y')
        elif method == 'logarithmic':
            y = (np.log(timeserie/timeserie.shift(1)).cumsum() + 1).reset_index(drop=True).replace(np.nan, 1).rename('y')
        else:
            raise Exception('Method must be "arithmetic" or "logarithmic"')
        x = pd.Series(range(0,len(y)), name='x')
        #x_ = x.mean()
        #y_ = y.mean()
        x_dev = x.std()
        y_dev = y.std()
        corr = pd.DataFrame([x,y]).T.corr().iloc[0,1]
        slope = corr * (y_dev/x_dev)
        #inter = y_ - slope*x_
        #reg = x * slope + inter
        return slope
       
    def trend_detection(self, timeserie, drift_threshold, method):
        slope = self.linreg(timeserie, method)
        if abs(slope) < drift_threshold:
            trend = LATERAL_MARKET
        else:
            trend = BULL_MARKET if np.sign(slope) == 1 else BEAR_MARKET
        return trend
        
    def trend_clasifier(self, timeserie_long, timeserie_short, drift_threshold, method):
        trend_short = self.trend_detection(timeserie_short, drift_threshold, method)
        trend_long = self.trend_detection(timeserie_long, drift_threshold, method)
        
        if trend_long == LATERAL_MARKET:
            result = trend_short
        elif trend_long == trend_short:
            result = trend_long
        else:
            result = LATERAL_MARKET
            
        return result
    
    def interval_detection(self, timeserie, interval_width_in_days, minutes_in_obs=240):
        obs_per_day = 1440 / minutes_in_obs
        number_of_intervals = int(len(timeserie)/obs_per_day/interval_width_in_days) 
        intervals = dict()
        end_date = timeserie.index[-1]
        start_date = end_date - timedelta(days=interval_width_in_days)
        for interval in range(1,number_of_intervals):
            name = 'interval_' + str(interval)
            intervals[name] = [start_date, end_date]
            end_date = end_date - timedelta(days=interval_width_in_days)
            start_date = end_date - timedelta(days=interval_width_in_days)
        return intervals
    
    def trend_clasifier_process(self, timeserie, drift_threshold, interval_width_in_days, minutes_in_obs, method,  plot=False):
        intervals = self.interval_detection(timeserie, interval_width_in_days, minutes_in_obs)
        results = dict()
        for k,v in intervals.items():
            timeserie_short = timeserie[ v[0] : v[1] ]
            timeserie_long = timeserie[ v[0] - timedelta(days=interval_width_in_days) : v[1] ]
            result = self.trend_clasifier(timeserie_long, timeserie_short, drift_threshold, method)
            results[k] = [v[0], v[1], result]
            
        if plot == True:
            fig, ax = plt.subplots(figsize=(12, 8))
            plt.title("Trend Clasifier")
            plt.plot(timeserie, "b--", label="Price")
            for k,v in results.items():
                if v[2] == BULL_MARKET:
                    ax.axvspan(v[0],v[1], color='g', linewidth=1, alpha=0.3)
                elif v[2] == BEAR_MARKET:
                    ax.axvspan(v[0],v[1], color='r',linewidth=1,alpha=0.3)
                else:
                    ax.axvspan(v[0],v[1], color='y',linewidth=1,alpha=0.3)
            plt.ylabel("Price")
            plt.xlabel("Time")
            plt.legend(loc='best')

        return results
    
    def get_market_tendency(self, pair):
        start_date = TODAY - timedelta(days=INTERVAL_WIDTH_IN_DAYS*3)
        
        timeserie = self.candlestick_repo.get_candlestick(pair,  'binance', MINUTES_IN_OBS, start_date, TODAY).close
        result = self.trend_clasifier_process(timeserie, DRIFT_THRESHOLD, INTERVAL_WIDTH_IN_DAYS, MINUTES_IN_OBS, METHOD)['interval_1']
        return result[2]
    
    def check_tendency_on_specific_date(self, timeserie, date, interval_width_in_days=1, minutes_in_obs=240, method='arithmetic', plot=False):
        
        end_date = date
        start_date = end_date - timedelta(days=interval_width_in_days*3)
        timeserie = timeserie[start_date:end_date]
        
        result = self.trend_clasifier_process(timeserie, DRIFT_THRESHOLD, interval_width_in_days, minutes_in_obs, method, plot)['interval_1']
        return result[2]
    
    def check_tendency_on_specific_dates_multiple(self,
                                                  pairs: list,
                                                  dates: list,
                                                  interval_width_in_days = 1,
                                                  minutes_in_obs=240,
                                                  method='arithmetic'):
        '''
        list = list of pairs to check in string format.
        dates = list of dates in datetime format.
        interval_width_in_days = days comprised in the short period, long period is the double.
        method = 'arithmetic' or 'logaritmic'.
        '''
        
        df = pd.DataFrame()
        for pair in pairs:        
            for date in dates:
                df.at[pair, str(date)] = self.check_tendency_on_specific_date(pair, date, interval_width_in_days, minutes_in_obs, method, False)
        return df.T                

