import numpy as np
from utils.xmetrics import Xmetrics
from ta.volatility import keltner_channel_lband,keltner_channel_hband
from utils.candlestick import CandlestickRepository
BinancePricesRepository = CandlestickRepository.default_repository
Xmetrics = Xmetrics()

class Holding:
    def __init__(self, candles):
        self.candles = candles
        self.periods = (self.candles.index[1]-self.candles.index[0]).seconds // 3600

    def process_candles(self):
        candles = self.candles        
        candles['signal'] = 1
        return candles        

    def get_performance(self):
        self.candles = self.process_candles()
        self.candles = Xmetrics.calculate_returns(self.candles)
        self.candles['performance'] = (self.candles['returns']+1).cumprod()
        return self.candles.performance[-1]
    
    def get_standard_bounds(self):
        return self.STANDARD_BOUNDS


class OutOfMarket(Holding):
    def process_candles(self):
        candles = self.candles        
        candles['signal'] = 0
        return candles        


class TwoStandardMovingAverage(Holding):    
    STANDARD_BOUNDS = {'ma_short': [1,50], 'ma_long': [51,100]}
    
    def process_candles(self, ma_short, ma_long):
        ma_short = int(ma_short)
        ma_long = int(ma_long)
        candles = self.candles        
        open_prices = candles.open 
  
        candles['short'] = open_prices.rolling(ma_short).mean()
        candles['long'] = open_prices.rolling(ma_long).mean()
        candles['signal'] = (candles.short >= candles.long).astype(int)

        return candles        
    
    def get_performance(self, ma_short, ma_long):
        self.candles = self.process_candles(ma_short, ma_long)
        self.candles = Xmetrics.calculate_returns(self.candles)
        self.candles['performance'] = (self.candles['returns']+1).cumprod()
        return self.candles.performance.iloc[-1]
    
    def get_xratio(self, ma_short, ma_long):
        if not 'performance' in self.candles.columns:
            self.candles = self.process_candles(ma_short, ma_long)
            self.candles = Xmetrics.calculate_returns(self.candles)
            self.candles['performance'] = (self.candles['returns']+1).cumprod()
        return Xmetrics.strategy_metrics(self.candles, self.periods).T['xratio'][0]


class ThreeStandardMovingAverage(Holding):
    STANDARD_BOUNDS = {'ma_short_buy': [1,50],
                       'ma_short_sell': [5,50],
                       'ma_long': [51,100]}
    
    def process_candles(self, ma_short_buy, ma_short_sell, ma_long):
        ma_short_buy = int(ma_short_buy)
        ma_short_sell = int(ma_short_sell)
        ma_long = int(ma_long)
        candles = self.candles        
        open_prices = candles.open 

        candles['short_buy'] = open_prices.rolling(ma_short_buy).mean()
        candles['short_sell'] = open_prices.rolling(ma_short_sell).mean()
        candles['long'] = open_prices.rolling(ma_long).mean()
        
        candles['signal'] = 0
        candles['signal'] = np.where(candles.short_buy>candles.long, 1, candles.signal)
        candles['signal'] = np.where(candles.long>candles.short_sell, -1, candles.signal)
        
        return candles

    def get_performance(self, ma_short_buy, ma_short_sell, ma_long):
        self.candles = self.process_candles(ma_short_buy, ma_short_sell, ma_long)
        self.candles = Xmetrics.calculate_returns(self.candles)
        self.candles['performance'] = (self.candles['returns']+1).cumprod()
        return self.candles.performance[-1]
    
    def get_xratio(self, ma_short_buy, ma_short_sell, ma_long):
        if not 'performance' in self.candles.columns:
            self.candles = self.process_candles(ma_short_buy, ma_short_sell, ma_long)
            self.candles = Xmetrics.calculate_returns(self.candles)
            self.candles['performance'] = (self.candles['returns']+1).cumprod()
        return Xmetrics.strategy_metrics(self.candles, self.periods).T['xratio'][0]
    
    
class ThreeStandardMovingAverageAlternative(Holding):
    STANDARD_BOUNDS = {'ma_short': [1,30],
                       'ma_medium': [25,60],
                       'ma_long': [51,100]}
    
    def process_candles(self, ma_short, ma_medium, ma_long):
        ma_short = int(ma_short)
        ma_medium = int(ma_medium)
        ma_long = int(ma_long)
        
        candles = self.candles
        candles['short'] = candles.open.rolling(ma_short).mean()
        candles['medium'] = candles.open.rolling(ma_medium).mean()
        candles['long'] = candles.open.rolling(ma_long).mean()
        
        candles['signal'] = 0
        candles['signal'] = np.where((candles.short>=candles.medium) & (candles.medium>=candles.long), 1, candles.signal)
        candles['signal'] = np.where((candles.short<=candles.medium) & (candles.medium<=candles.long), -1, candles.signal)
        
        return candles

    def get_performance(self, ma_short, ma_medium, ma_long):
        self.candles = self.process_candles(ma_short, ma_medium, ma_long)
        self.candles = Xmetrics.calculate_returns(self.candles)
        self.candles['performance'] = (self.candles['returns']+1).cumprod()
        return self.candles.performance[-1]
    
    def get_xratio(self, ma_short, ma_medium, ma_long):
        if not 'performance' in self.candles.columns:
            self.candles = self.process_candles(ma_short, ma_medium, ma_long)
            self.candles = Xmetrics.calculate_returns(self.candles)
            self.candles['performance'] = (self.candles['returns']+1).cumprod()
        return Xmetrics.strategy_metrics(self.candles, self.periods).T['xratio'][0]
    
    
class ChandelierExitStrategy(Holding): 
    STANDARD_BOUNDS = {'mean': [10,30],
                       'chand_window': [5,15],
                       'mult_high': [0.01,5],
                       'mult_low': [0.01,5]}
    
    def calculate_true_range(self):
        candles = self.candles
        candles['tr1'] = candles["high"] - candles["low"]
        candles['tr2'] = abs(candles["high"] - candles["close"].shift(1))
        candles['tr3'] = abs(candles["low"] - candles["close"].shift(1))
        candles['TR'] = candles[['tr1','tr2','tr3']].max(axis=1)
        candles.loc[candles.index[0],'TR'] = 0
        return candles

    def calculate_average_true_range(self, mean):
        candles = self.calculate_true_range()
        candles['ATR'] = 0
        candles.loc[candles.index[mean],'ATR'] = round( candles.loc[candles.index[1:mean+1],"TR"].rolling(window=mean).mean()[-1], 4)
        const_atr = (mean-1)/mean
        const_tr = 1/mean
        ATR=candles["ATR"].values
        TR=candles["TR"].values
        for index in range(mean+1, len(candles)):
            ATR[index]=ATR[index-1]*const_atr+TR[index]*const_tr
        candles["ATR"]=ATR
        return candles
    
    def calculate_chandelier_exits(self, mean, chand_window, mult_high, mult_low):
        candles = self.calculate_average_true_range(mean)
        candles["chandelier_high"] = candles['close']
        candles["chandelier_low"] = candles['close']
        candles.loc[candles.index[chand_window+1:],"chandelier_low"] = candles.loc[candles.index[chand_window+1:],"low"].rolling(chand_window).min() + mult_low * candles["ATR"][chand_window+1:]
        candles.loc[candles.index[chand_window+1:],"chandelier_high"] = candles.loc[candles.index[chand_window+1:],"high"].rolling(chand_window).max() - mult_high * candles["ATR"][chand_window+1:]
        return candles

    def process_candles(self, mean, chand_window, mult_high, mult_low):
        mean = int(mean)
        chand_window = int(chand_window)
        candles = self.candles
        
        candles = self.calculate_chandelier_exits(mean, chand_window, mult_high, mult_low)
        candles["signal"] = 0
        candles["signal"] = np.where(candles["close"] > candles["chandelier_low"], 1, candles["signal"])
        candles["signal"] = np.where(candles["close"] < candles["chandelier_high"], -1, candles["signal"])
        candles["signal"] = candles["signal"].shift(1).fillna(0) 
        return candles
    
    def get_performance(self, mean, chand_window, mult_high, mult_low):
        self.candles = self.process_candles(mean, chand_window, mult_high, mult_low)
        self.candles = Xmetrics.calculate_returns(self.candles)
        self.candles['performance'] = (self.candles['returns']+1).cumprod()
        return self.candles.performance[-1]
    
    def get_xratio(self, mean, chand_window, mult_high, mult_low):
        if not 'performance' in self.candles.columns:
            self.candles = self.process_candles(mean, chand_window, mult_high, mult_low)
            self.candles = Xmetrics.calculate_returns(self.candles)
            self.candles['performance'] = (self.candles['returns']+1).cumprod()
        return Xmetrics.strategy_metrics(self.candles, self.periods).T['xratio'][0]
    
    
class VWAPvsSMA(Holding):
    STANDARD_BOUNDS = {'n_candles': [1,10],
                       'min_percentage': [0,0.1]}
    
    def calculate_vwap(self, n_candles):
        candles = self.candles
        volume_times_price_mean = (candles.volume * candles.open).mean()
        volume_mean = candles.volume.shift().rolling(n_candles).mean()
        return volume_times_price_mean / volume_mean
    
    def process_candles(self, n_candles, min_percentage):
        candles = self.candles
        n_candles = int(n_candles) 
        vwap = self.calculate_vwap(n_candles)
        sma = candles.open.rolling(n_candles).mean()
        current_percentage = abs(np.log(vwap/sma))

        candles['signal'] = 0
        candles['signal'] = np.where((current_percentage >= min_percentage) & (vwap > sma), 1, candles["signal"])
        
        return candles

    def get_performance(self, n_candles, min_percentage):
        self.candles = self.process_candles(n_candles, min_percentage)
        self.candles = Xmetrics.calculate_returns(self.candles)
        self.candles['performance'] = (self.candles['returns']+1).cumprod()
        return self.candles.performance[-1]
    
    def get_xratio(self, n_candles, min_percentage):
        if not 'performance' in self.candles.columns:
            self.candles = self.process_candles(n_candles, min_percentage)
            self.candles = Xmetrics.calculate_returns(self.candles)
            self.candles['performance'] = (self.candles['returns']+1).cumprod()
        return Xmetrics.strategy_metrics(self.candles, self.periods).T['xratio'][0]
    
    
class BBvolumeStrategy(Holding): 
    STANDARD_BOUNDS = {'bb_periods': [1,20],
                       'bb_mult_factor': [1,5],
                       'vol_periods': [1,20],
                       'vol_mult_factor_down': [1,5],
                       'vol_mult_factor_up': [1,5]}
    
    def process_candles(self, bb_periods, bb_mult_factor, vol_periods, vol_mult_factor_down, vol_mult_factor_up):
        candles = self.candles        
        bb_periods = int(bb_periods)
        vol_periods = int(vol_periods) 
                
        candles["volume"] = candles["volume"].shift(1)
        candles["high"] = candles["high"].shift(1)
        candles["low"] = candles["low"].shift(1)
        candles["sma"] = candles["open"].rolling(bb_periods).mean()
        candles["std"] = candles["open"].rolling(bb_periods).std()
        candles["bbu"] = candles["sma"] + bb_mult_factor*candles["std"]
        candles["bbl"] = candles["sma"] - bb_mult_factor*candles["std"]
        candles["kcu"] = keltner_channel_hband(high=candles["high"],
                                               low=candles["low"],
                                               close=candles["open"],
                                               n=bb_periods)
        candles["kcl"] = keltner_channel_lband(high=candles["high"],
                                               low=candles["low"],
                                               close=candles["open"],
                                               n=bb_periods)
        candles["vol_change"] = candles["volume"].rolling(vol_periods).mean()
        
        candles["signal"] = np.nan
        candles["signal"] = np.where((candles["bbu"]<candles["close"]) & \
             (candles["kcu"]<candles["close"]) & \
             ((candles["bbu"]>candles["close"].shift(1)) |\
             (candles["kcu"]>candles["close"].shift(1))) &\
             (candles["volume"]>vol_mult_factor_up*candles["vol_change"])\
             , 2,candles["signal"])
        candles["signal"] = np.where((candles["bbl"]>candles["close"]) & \
             (candles["kcl"]>candles["close"]) & \
             ((candles["bbl"]<candles["close"].shift(1)) |\
             (candles["kcl"]<candles["close"].shift(1))) &\
             (candles["volume"]<vol_mult_factor_down*candles["vol_change"])\
             ,1, candles["signal"])
        candles["signal"] = np.where((candles["bbu"]<candles["close"]) &\
             (candles["kcu"]<candles["close"]) &\
             ((candles["bbu"]>candles["close"].shift(1)) |\
             (candles["kcu"]>candles["close"].shift(1))) &\
             (candles["volume"]<vol_mult_factor_up*candles["vol_change"])\
             ,-1, candles["signal"])
        candles["signal"] = np.where((candles["bbl"]>candles["close"]) &\
             (candles["kcl"]>candles["close"]) &\
             ((candles["bbl"]<candles["close"].shift(1)) |\
             (candles["kcl"]<candles["close"].shift(1))) &\
             (candles["volume"]>vol_mult_factor_down*candles["vol_change"])\
             , -2, candles["signal"])
       
        status = candles["signal"].ffill()
        candles["signal"] = np.where((status==2) &\
             (candles["sma"]>=candles["close"])
             ,0, candles["signal"])
        candles["signal"] = np.where((status==-2) &\
             (candles["sma"]<=candles["close"])
             ,0, candles["signal"])
         
        candles["signal"]=candles["signal"].replace(2,1).replace(-2,-1)
        candles["signal"]=candles["signal"].shift(1).ffill().replace(np.nan,0)
        return candles
    
    def get_performance(self, bb_periods, bb_mult_factor, vol_periods, vol_mult_factor_down, vol_mult_factor_up):
        self.candles = self.process_candles(bb_periods, bb_mult_factor, vol_periods, vol_mult_factor_down, vol_mult_factor_up)
        self.candles = Xmetrics.calculate_returns(self.candles)
        self.candles['performance'] = (self.candles['returns']+1).cumprod()
        return self.candles.performance[-1]
    
    def get_xratio(self, bb_periods, bb_mult_factor, vol_periods, vol_mult_factor_down, vol_mult_factor_up):
        if not 'performance' in self.candles.columns:
            self.candles = self.process_candles(bb_periods, bb_mult_factor, vol_periods, vol_mult_factor_down, vol_mult_factor_up)
            self.candles = Xmetrics.calculate_returns(self.candles)
            self.candles['performance'] = (self.candles['returns']+1).cumprod()
        return Xmetrics.strategy_metrics(self.candles, self.periods).T['xratio'][0]
