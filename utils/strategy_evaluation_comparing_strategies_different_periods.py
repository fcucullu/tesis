# By FranCu

import pandas as pd
from datetime import datetime
import warnings
warnings.filterwarnings('ignore')
from utils.candlestick import CandlestickRepository
from utils.xcapit_metrics import Xmetrics
import utils.simulations as sim
from utils.strategies import TwoStandardMovingAverage, ThreeStandardMovingAverage, ThreeStandardMovingAverageAlternative, ChandelierExitStrategy, VWAPvsSMA, BBvolumeStrategy
from utils.optimizators import bayes_optimize

###############################################################################
'''                                    INPUTS                               '''
###############################################################################

Xmetrics = Xmetrics()
Repository = CandlestickRepository.default_repository()

###PERIOD TOP STUDY !###
start_date = datetime(2021,1,1)
end_date = datetime(2022,1,1)
minutes = 60*4
side_market = 'bajista'
currency = 'BTC'
ticker = currency+'/USDT'
###PERIOD TOP STUDY !###

candles = Repository.get_candlestick(ticker,
                                               'binance', 
                                               minutes,
                                               start_date,
                                               end_date)
candles = sim.FillNa().fill_ohlc(candles)    

###############################################################################
'''                    two_moving_average_strategy                          '''
###############################################################################

bounds={'ma_short': [1,50], 'ma_long': [51,100]}
strategy = TwoStandardMovingAverage(candles)  
func = strategy.get_performance    

results = bayes_optimize(func, bounds, True)
print(pd.DataFrame.from_dict(results.max))

###############################################################################
'''                    three_moving_average_strategy                        '''
###############################################################################

bounds={'ma_short_buy': [1,50],
        'ma_short_sell': [5,50],
        'ma_long': [51,100]}
strategy = ThreeStandardMovingAverage(candles)
func = strategy.get_performance    

results = bayes_optimize(func, bounds, True)
print(pd.DataFrame.from_dict(results.max))

###############################################################################
'''                    three_moving_average_strategy v2                     '''
###############################################################################

bounds={'ma_short': [1,30],
        'ma_medium': [25,60],
        'ma_long': [51,100]}
strategy = ThreeStandardMovingAverageAlternative(candles)
func = strategy.get_performance    

results = bayes_optimize(func, bounds,True)
print(pd.DataFrame.from_dict(results.max))

###############################################################################
'''                    Chandelier Exits Strategy                           '''
###############################################################################

bounds={'mean': [10,30],
        'chand_window': [5,15],
        'mult_high': [0.01,5],
        'mult_low': [0.01,5]}

strategy = ChandelierExitStrategy(candles)
func = strategy.get_performance    

results = bayes_optimize(func, bounds, True)
print(pd.DataFrame.from_dict(results.max))

###############################################################################
'''                         VWAP vs SMA                                    '''
###############################################################################

bounds={'n_candles': [1,10],
        'min_percentage': [0,0.1]}

strategy = VWAPvsSMA(candles)
func = strategy.get_performance    

results = bayes_optimize(func, bounds, True)
print(pd.DataFrame.from_dict(results.max))

###############################################################################
'''                         BBvolumeStrategy                                '''
###############################################################################

bounds={'bb_periods': [1,20],
        'bb_mult_factor': [1,5],
        'vol_periods': [1,20],
        'vol_mult_factor_down': [1,5],
        'vol_mult_factor_up': [1,5],
        }

strategy = BBvolumeStrategy(candles)
func = strategy.get_performance    

results = bayes_optimize(func, bounds, True)
print(pd.DataFrame.from_dict(results.max))

































# ###############################################################################
# '''                    now_better_than_before                              '''
# ###############################################################################

# parameters={'alcista': [.97, 1, 1.027],
#             'lateral': [1, 1, 1.05],
#             'bajista': [1, 1, 1.05]}

# strategy = BullMarketNowBetterThanBefore(base_currency=currency,
#                                          quote_currency='USDT',
#                                          time_frame=minutes,
#                                          bull_market_factor=parameters[side_market][0],
#                                          bear_market_factor=parameters[side_market][1])
    
# nbtb = strategy.process_candles(candles)
# nbtb = strategy.check_threshold_and_takeprofit(nbtb)    
# nbtb = Xmetrics.calculate_returns(nbtb)
# nbtb['performance'] = (nbtb['returns']+1).cumprod()

# ###############################################################################
# '''                 dynamic_now_better_than_before                          '''
# ###############################################################################
    
# strategy = BullMarketNowBetterThanBeforeDynamicParameters(base_currency=currency,
#                                          quote_currency='USDT',
#                                          time_frame=minutes,
#                                          )
    
# d_nbtb = strategy.process_candles(candles)
# d_nbtb = strategy.check_threshold_and_takeprofit(d_nbtb)    
# d_nbtb = Xmetrics.calculate_returns(d_nbtb)
# d_nbtb['performance'] = (d_nbtb['returns']+1).cumprod()


# ###############################################################################
# '''                 dynamic_now_better_than_before w/MA100                          '''
# ###############################################################################

# strategy = BullMarketNowBetterThanBeforeDynamicParameters(base_currency=currency,
#                                          quote_currency='USDT',
#                                          time_frame=minutes,
#                                          )
    
# d_nbtb_ma100 = strategy.process_candles(candles)
# d_nbtb_ma100['basic_signal'] = np.where(d_nbtb_ma100.open >= d_nbtb_ma100.open.rolling(100).mean(), d_nbtb_ma100['basic_signal'], 0)
# d_nbtb_ma100 = strategy.check_threshold_and_takeprofit(d_nbtb_ma100)    
# d_nbtb_ma100 = Xmetrics.calculate_returns(d_nbtb_ma100)
# d_nbtb_ma100['performance'] = (d_nbtb_ma100['returns']+1).cumprod()


# ###############################################################################
# '''                            T R E N D   F O L L O W E R                 '''
# ###############################################################################

# strategy = TrendFollowerStrategy(base_currency=currency,
#                                              quote_currency='USDT',
#                                              time_frame=minutes,
#                                              )

# tf = strategy.process_candles(candles)
# tf = Xmetrics.calculate_returns(tf)
# tf['performance'] = (tf['returns']+1).cumprod()

# ###############################################################################
# '''                 COMPARACION  DE  METRICAS                               '''
# ###############################################################################
# plt.figure(figsize=(12,10))
# plt.plot((candles.open.pct_change()+1).cumprod(), label="Holding")
# plt.plot(nbtb.performance, label="NBTB")
# plt.plot(d_nbtb.performance, label="D_NBTB")
# plt.plot(d_nbtb_ma100.performance, label="D_NBTB_w/MA100")
# plt.plot(tf.performance, label="Trend Follower")
# plt.legend()
# plt.show()

# Xmetrics.strategy_metrics(nbtb, periods, 'signal')
# Xmetrics.strategy_metrics(d_nbtb, periods, 'signal')
# Xmetrics.strategy_metrics(d_nbtb_ma100, periods, 'signal')
# Xmetrics.strategy_metrics(tf, periods, 'signal')

